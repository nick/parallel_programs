#!/bin/sh

#SBATCH --account=csmpistud
#SBATCH --partition=csmpi_fpga_short
#SBATCH --gres=gpu:nvidia_a30:1
#SBATCH --time=0:05:00
#SBATCH --output=opencl.out

export XILINX_XRT=/opt/xilinx/xrt

#Force a recompile by emptying the binaries folder.
make clean
#Compile on the machine, not the head node
BUILD=RELEASE make bin/square_cl

bin/square_cl 32> results/square.txt
