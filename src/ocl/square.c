/* See ocl-simple for a more verbose version. This prints only the time in
 * seconds to allow for easy benchmarking. */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <CL/cl.h>
#include "simple.h"

#define DATA_SIZE 10240000

int main (int argc, char * argv[])
{
  cl_kernel kernel;
  size_t global[1];
  size_t local[1];
  /* Must be run from top directory, otherwise this path is wrong! */
  char *KernelSource = readOpenCL( "src/ocl/square.cl");

  if (argc != 2) {
      printf("Usage: %s <workgroup size>\n", argv[0]);
      return EXIT_FAILURE;
  }

  local[0] = atoi(argv[1]);

  fprintf(stderr, "work group size: %d\n", (int)local[0]);

  /* Create data for the run.  */
  float *data = NULL;                /* Original data set given to device.  */
  float *results = NULL;             /* Results returned from device.  */
  int correct;                       /* Number of correct results returned.  */

  int count = DATA_SIZE;
  global[0] = count;

  data = (float *) malloc (count * sizeof (float));
  results = (float *) malloc (count * sizeof (float));

  /* Fill the vector with random float values.  */
  for (int i = 0; i < count; i++)
    data[i] = rand () / (float) RAND_MAX;

  CL_SAFE(initGPU());

  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  kernel = setupKernel( KernelSource, "square", 3, FloatArr, count, data,
                                                   FloatArr, count, results,
                                                   IntConst, count);

  runKernel( kernel, 1, global, local);

  gettimeofday(&tv2, NULL);

  double duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1e6 +
                    (double) (tv2.tv_sec - tv1.tv_sec) / 1e9;

  double flops = (double)DATA_SIZE;
  printf("%lf\n", flops / duration / 1e9);

  /* Validate our results.  */
  correct = 0;
  for (int i = 0; i < count; i++)
    if (results[i] == data[i] * data[i])
      correct++;

  /* Print a brief summary detailing the results.  */
  fprintf (stderr, "Computed %d/%d %2.0f%% correct values\n", correct, count,
                   (float)correct/count*100.f);

  CL_SAFE(clReleaseKernel (kernel));
  CL_SAFE(freeDevice());

  free(data);
  free(results);

  return 0;
}
